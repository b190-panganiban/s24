// 1.


let getCube  = 2 ** 3;
console.log(`The cube of 2 is ${getCube }`);


// 2.
const fullAddress = [258, 90011];

const [houseNumber, houseZipcode ] = fullAddress;
console.log(`I live at ${houseNumber} Washington Ave NW, California ${houseZipcode}`);


// 3.
const animal = {
   animalName: "Lolong",
   animalType: "saltwater",
   animalWeight: 1075,
   animalFeet: 20,
   animalInches:3
};
console.log(`${animal.animalName} was a ${animal.animalType} crocodile. He weighed at ${animal.animalWeight} kgs with a measurement of ${animal.animalFeet} ft ${animal.animalInches} in.`);



// 4.
const printNumbers = [ 1, 2, 3, 4, 5];
printNumbers.forEach(function(printNumbers){
   console.log(printNumbers);
});


// 5.
const minus = ( x, y ) => x - y;
let reduceNumber  = minus(20, 5);
console.log(reduceNumber );



// 6.
class Dog{
   constructor (name, age, breed){
      this.name = name;
      this.age = age;
      this.breed = breed;
   }
};

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachsund";

console.log(myDog);